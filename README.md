# Jitsi Meet on Docker



## Some basic commands 

  kubernets all files on below path 

  cd docker-jitsi/docker-jitsi-meet/examples/kubernetes/ 

  vi deployment.yaml
  kubectl  apply -f deployment.yaml
  vi deployment.yaml
  kubectl  apply -f deployment.yaml

   kubectl get pod -n jitsi
   kubectl describe  pods jitsi-59d88d469b-ggxj9  -n jitsi
   kubectl get pod -n jitsi
   kubectl describe  pods jitsi-5879c47c89-872dh  -n jitsi



   kubectl get secret objectstore-cert -n jitsi
   kubectl describe secret objectstore-cert -n jitsi
   kubectl create secret generic cert_28102020 --from-file=cert -n jistsi
   kubectl create secret generic cert_28102020 --from-file=cert -n jitsi
   kubectl create secret generic cert-28102020 --from-file=cert -n jitsi
   kubectl describe secret  cert-28102020 -n jitsi

![](resources/jitsi-docker.png)

[Jitsi](https://jitsi.org/) is a set of Open Source projects that allows you to easily build and deploy secure videoconferencing solutions.

[Jitsi Meet](https://jitsi.org/jitsi-meet/) is a fully encrypted, 100% Open Source video conferencing solution that you can use all day, every day, for free — with no account needed.

This repository contains the necessary tools to run a Jitsi Meet stack on [Docker](https://www.docker.com) using [Docker Compose](https://docs.docker.com/compose/).

## Installation

The installation manual is available [here](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker).

## TODO

* Support container replicas (where applicable).
* TURN server.

